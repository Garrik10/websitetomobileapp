Generating a basic application from the URL of a website
1- prerequisites:
 - jdk;
  -sdk;
  -Android studio
 
2- Create new app with Android studio;

3-  Open the file app/src/main/res/layout/activity_main.xml, create a webview with the desired dimensions and assign an ID to the webView. It is also possible to draw this webView using to the Design tool proposed by Android studio for xml files.

4- Open the file MainActivity.java (app/src/main/java/{package name}/MainActivity.java). Create an attribute of type WebView (package android.webkit.WebView);

5- In the MainActivity.java file, override the function onCreate as follows:

@Override

protected void onCreate(Bundle savedInstanceState) {
	
super.onCreate(savedInstanceState);
		
setContentView(R.layout.activity_main);
		
myWebView = (WebView) findViewById(R.id.webView);          // the word webView following  the keyword R.id in brackets is the ID of the webview created in step 3.
		
WebSettings webSettings = myWebView.getSettings();
		
webSettings.setJavaScriptEnabled(true);
		
myWebView.loadUrl("https://www.drilinstore.com/");                // The URL of the website from which the mobile application is to be created.
		
myWebView.setWebViewClient(new WebViewClient());
		
}

These packages have to be imported:
android.support.v7.app.AppCompatActivity;
android.os.Bundle;
android.webkit.WebSettings;
android.webkit.WebView;
android.webkit.WebViewClient;
	
6- Allow internet access by adding the following line <uses-permission android:name="android.permission.INTERNET"> </uses-permission> in the file app/src/main/AndroidManifest.xml;

7- Build application.

8- A complete example is available in the folder ''drilinstore".